package com.ecommerce.auth.services.impl;

import com.ecommerce.auth.mappers.UserMapper;
import com.ecommerce.auth.model.dto.UserDto;
import com.ecommerce.auth.model.dto.UserResponseDto;
import com.ecommerce.auth.repository.UserRepository;
import com.ecommerce.auth.services.UserService;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;

/**
 * UserServiceImpl .
 *
 * @author Carlos
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;

  @Override
  public Mono<UserResponseDto> saveUser(final UserDto userDto) {
    return Mono.fromCallable(() -> UserMapper.userDtoToUser(userDto))
        .flatMap(userRepository::save)
        .map(UserMapper::userToUserResponseDto);
  }

}
