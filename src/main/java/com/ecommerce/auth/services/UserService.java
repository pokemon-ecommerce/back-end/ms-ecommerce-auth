package com.ecommerce.auth.services;

import com.ecommerce.auth.model.dto.UserDto;
import com.ecommerce.auth.model.dto.UserResponseDto;

import reactor.core.publisher.Mono;

/**
 * UserService .
 *
 * @author Carlos
 */
public interface UserService {
  Mono<UserResponseDto> saveUser(UserDto userDto);
}
