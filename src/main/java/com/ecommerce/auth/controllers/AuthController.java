package com.ecommerce.auth.controllers;

import com.ecommerce.auth.model.dto.UserDto;
import com.ecommerce.auth.model.dto.UserResponseDto;
import com.ecommerce.auth.services.impl.UserServiceImpl;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

/**
 * AuthController.
 *
 * @author Carlos
 */
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
  private final UserServiceImpl userService;

  @PostMapping
  public Mono<UserResponseDto> saveUser(@RequestBody final UserDto userDto) {
    return userService.saveUser(userDto);
  }

}
