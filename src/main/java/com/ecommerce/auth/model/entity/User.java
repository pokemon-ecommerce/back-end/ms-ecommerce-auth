package com.ecommerce.auth.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * User .
 *
 * @author Carlos
 */
@Table("users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class User {
  @Id
  private Long id;
  private String email;
  private String password;
  @Column("is_admin")
  @Builder.Default
  private Boolean isAdmin = false;
}
