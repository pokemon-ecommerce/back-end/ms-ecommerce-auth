package com.ecommerce.auth.repository;

import com.ecommerce.auth.model.entity.User;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

/**
 * User .
 *
 * @author Carlos
 */
@Repository
public interface UserRepository extends ReactiveCrudRepository<User, Long> {

}
