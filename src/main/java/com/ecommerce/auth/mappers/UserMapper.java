package com.ecommerce.auth.mappers;

import com.ecommerce.auth.model.dto.UserDto;
import com.ecommerce.auth.model.dto.UserResponseDto;
import com.ecommerce.auth.model.entity.User;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * UserService .
 *
 * @author Carlos
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class UserMapper {

  /**
   * userDtoToUser.
   *
   * @param userDto .
   * @return
   */
  public static User userDtoToUser(final UserDto userDto) {
    return User.builder()
        .email(userDto.getEmail())
        .password(userDto.getPassword())
        .build();
  }

  /**
   * userToUserDto.
   *
   * @param user .
   * @return
   */
  public static UserResponseDto userToUserResponseDto(final User user) {
    return UserResponseDto.builder()
        .id(user.getId())
        .email(user.getEmail())
        .isAdmin(user.getIsAdmin())
        .build();
  }

}
